resource "aws_key_pair" "key" {
  key_name   = "aws-key"
  public_key = file("./aws-key.pub")
  tags = {
    key = "ec2"
  }
}

resource "aws_instance" "ec2_terraform" {
  ami           = "ami-053b0d53c279acc90" # us-east-1
  instance_type = "t2.micro"
  key_name      = aws_key_pair.key.key_name
  subnet_id     = module.mod-vpc.subnet_id
  #vpc_security_group_ids      = module.mod-vpc.security_group_id
  associate_public_ip_address = true
  tags = {
    Name = "ec2-dev"
  }

}