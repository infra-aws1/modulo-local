terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.12.1"
    }
  }
}
provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      owner      = "leopoldo_cardoso"
      managed_by = "terraform"
      objective  = "estudos_terraform"
    }
  }
}

module "mod-vpc" {
  source = "./modulo-vpc" # Não é necessário está dentro da mesma pasta

  cidr_vpc    = "10.0.0.0/16"
  cidr_subnet = "10.0.1.0/24"
  environment = "dev"
}